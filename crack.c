#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    // Compare the two hashes
    char *hashguess = md5(guess, strlen(guess) );
    //printf("hashguess is: %s\n", hashguess);
    if( strcmp(hashguess, hash) == 0) {
        return 1;
    }
    else
        return 0;
    // Free any malloc'd memory (ignore)
    
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    FILE *fp = fopen(filename, "r");
    if(!fp) {
        printf("File %s cannot be opened", filename );
        exit(1);
    }
    
    
    struct stat info; 
    if ( stat(filename, &info) == -1 ) {
        printf("Can't stat the file\n");
        exit(1);
    }
    
    int filesize = info.st_size;
    //printf("File is %d bytes\n", filesize); //test:works
    
    //allocating mem for file +1 for null
    char *contents=malloc(filesize+1);
    fread(contents, 1, filesize, fp);
    fclose(fp);
    contents[filesize] = '\0';
    
    //grabbing linecount
    int nlcount=0;
    for( int i = 0; i < filesize; i++) {
        if (contents[i] == '\n') nlcount++;
    }
    
    //printf("Line count is %d\n", nlcount); //test:works
    
    char ** lines = malloc(nlcount * sizeof(char * ) );
    
    //fill in array of string pointers
    lines[0] = strtok(contents, "\n");
    int i =1;
    while( (lines[i] = strtok(NULL, "\n")) != NULL) 
    {
        i++;
    }
    
    
    //print out each line seperately
    /* 
    for (int j=0; j < nlcount; j++) {
        printf("%d %s\n", j+1, lines[j] );
    } */ //test:works
    
    //key vars
    //contents = points to entire file
    //lines = pointer to array of strings
    //nlcount = number of strings
    
    *size = nlcount;
    return lines;
}


int main(int argc, char *argv[])
{ //argv[1] has hashes, argv[2] has rockyou
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *hashfile = fopen(argv[1], "r");
    if (!hashfile) {
        printf("Can't open hash file\n");
    }
    //printf("dlen is: %d\n", dlen );
    
    char words[100]; 
    while ( fgets(words, 100, hashfile) != NULL )
    {
        //printf("words is: %s", words);
        words[strlen(words)-1] = '\0';
        for( int i=0; i < dlen; i++) 
        {
            if( tryguess(words, dict[i] ) == 1) {
                printf("Got it: %s,%s\n", dict[i], words );
            }
        } 
    }  
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    
    free(dict[0]);
    free(dict);
}
